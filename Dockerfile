FROM ubuntu:16.04
LABEL Description="This image is used to build Boostcoin from github master"

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu trusty main" > /etc/apt/sources.list.d/bitcoin.list

RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev libqrencode-dev git-core libevent-dev libboost-all-dev libdb++-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN git clone https://github.com/mammix2/boostcoin-core.git /opt/boostcoin && \
    cd /opt/boostcoin/src && \
    make -f makefile.unix  && \
    cp /opt/boostcoin/src/boostcoind /usr/local/bin/ && \
    rm -rf /opt/boostcoin

RUN groupadd -r boostcoin && useradd -r -m -g boostcoin boostcoin

ENV BOOSTCOIN_DATA /data

RUN mkdir $BOOSTCOIN_DATA

COPY boostcoin.conf $BOOSTCOIN_DATA/boostcoin.conf

RUN chown boostcoin:boostcoin $BOOSTCOIN_DATA && \
    ln -s $BOOSTCOIN_DATA /home/boostcoin/.boostcoin

USER boostcoin
VOLUME /data

EXPOSE 9697 9698

CMD ["/usr/local/bin/boostcoind", "-conf=/data/boostcoin.conf", "-server", "-txindex", "-printtoconsole", "-reindex"]