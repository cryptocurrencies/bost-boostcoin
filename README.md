This Dockerfile generates the latest boostcoind client. It is anticiapted that you would run the following to install it on your server:

Replacing /localdata with a location on your server where you want to keep the persistant data:
```sh
docker run -d -P --name boostcoin -v /localdata:/data \
registry.gitlab.com/cryptocurrencies/bost-boostcoin:latest
```
